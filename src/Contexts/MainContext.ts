import React from "react";
import { ContextData, UserView } from "../Utilities/Types";
const data: ContextData = {};

export const MainContext = React.createContext({
	data,
	setUser: (user: UserView) => {},
});
