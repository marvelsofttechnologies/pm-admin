import React from "react";

export default function AddRep() {
	return (
		<div>
			<div className="flex-item">
				<div className="item-page">Add a New Rep</div>
				<i className="fal fa-times cancel" />
			</div>
			<form className="mt-5">
				<div className="rep-image mx-auto w-fit mb-5">
					<div className="rep-profile-img lg" />
				</div>
				<div className="input-box">
					<div className="input-label">First Name</div>
					<input type="text" className="formfield" placeholder="" />
				</div>
				<div className="input-box">
					<div className="input-label">Surname</div>
					<input type="text" className="formfield" placeholder="" />
				</div>
				<div className="input-box">
					<div className="input-label">Email</div>
					<input type="text" className="formfield" placeholder="" />
				</div>
				<button className="secondary-btn">Ok</button>
			</form>
		</div>
	);
}
