import React from "react";

export default function ViewProperty() {
	return (
		<div>
			<div className="flex-item">
				<div className="item-page">View Property</div>
				<i className="fal fa-times cancel" />
			</div>
			<div className="property-info-box">
				<div className="property-img">
					<img src="/assets/House_Tour_Liverman_3D6A3138_tour.0.jpg" alt="" />
					<div className="listing-location">Lekki Phase 1</div>
				</div>
				<div className="property-details-box">
					<div className="flex-item mb-4">
						<h6 className="property-box-title">4 Bedroom Terrace with Pool</h6>
						<i className="fas fa-badge-check b" />
					</div>
					<div className="flex-item cl">
						<p className="property-box-sub">
							<i className="fal fa-bed" /> 4 Bedrooms
						</p>
						<p className="property-box-sub">
							<i className="fal fa-toilet" /> 4 Bathrooms
						</p>
						<p className="property-box-sub">
							<i className="fal fa-tags" /> ₦145M
						</p>
						<p className="property-box-sub">
							<i className="fal fa-award" /> Governor’s Consent
						</p>
					</div>
					<h6 className="desc-title">Description</h6>
					<ul className="desc">
						<li className="description-text">Fully-fitted kitchen cabinets</li>
						<li className="description-text">Fully-fitted kitchen cabinets</li>
					</ul>
					<h6 className="desc-title">Pictures</h6>
					<div className="flex">
						<div className="pic-tab">
							<img src="" alt="" />
						</div>
						<div className="pic-tab">
							<img src="" alt="" />
						</div>
					</div>
					<h6 className="desc-title">Interactive 3D Tour</h6>
					<div className="flex">
						<div className="pic-tab w-100" />
					</div>
				</div>
			</div>
		</div>
	);
}
