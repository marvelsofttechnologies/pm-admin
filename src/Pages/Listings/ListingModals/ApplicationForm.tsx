import React from "react";

export default function ApplicationForm() {
	return (
		<div>
			<div className="flex-item">
				<div className="item-page">Application</div>
				<i className="fal fa-times cancel" />
			</div>
			<div className="row">
				<div className="col-lg-6">
					<div className="property-info-box">
						<div className="input-box">
							<div className="input-label">First Name</div>
							<input type="text" className="formfield" placeholder="Pade" />
						</div>
						<div className="input-box">
							<div className="input-label">Middle Name</div>
							<input type="text" className="formfield" placeholder="Emmanuel" />
						</div>
						<div className="input-box">
							<div className="input-label">Surname</div>
							<input
								type="text"
								className="formfield"
								placeholder="Aladejobi"
							/>
						</div>
						<div className="input-box">
							<div className="input-label">Mobile Number</div>
							<input
								type="text"
								className="formfield"
								placeholder="080 1234 5678"
							/>
						</div>
						<div className="input-box">
							<div className="input-label">Email Address</div>
							<input
								type="text"
								className="formfield"
								placeholder="padeomotosho@gmail.com"
							/>
						</div>
						<div className="input-box">
							<div className="input-label">Residential Address</div>
							<input
								type="text"
								className="formfield"
								placeholder="10 Breadfruit Street, Lagos Island, Lagos Nigeria"
							/>
						</div>
						<div className="input-box">
							<div className="input-label">Date of Birth</div>
							<input type="text" className="formfield" placeholder="13/04/93" />
						</div>
						<div className="input-box">
							<div className="input-label">Nationality</div>
							<div className="select-box">
								<select className="formfield">
									<option>Choose an option</option>
								</select>
								<div className="arrow" />
							</div>
						</div>
						<div className="input-box">
							<div className="input-label">Marital status</div>
							<div className="select-box">
								<select className="formfield">
									<option>Choose an option</option>
								</select>
								<div className="arrow" />
							</div>
						</div>
						<div className="input-box">
							<div className="input-label">Occupation</div>
							<input type="text" className="formfield" placeholder="Lawyer" />
						</div>
					</div>
				</div>
				<div className="col-lg-6">
					<div className="property-info-box">
						<div className="input-box">
							<div className="input-label">Employer</div>
							<input
								type="text"
								className="formfield"
								placeholder="1st Trust Bank Limited"
							/>
						</div>
						<div className="input-box">
							<div className="input-label">Work Address</div>
							<input
								type="text"
								className="formfield"
								placeholder="33B Ron Road Oyi Lagos"
							/>
						</div>
						<div className="kin-head">Next of Kin</div>
						<div className="input-box">
							<div className="input-label">First Name</div>
							<input type="text" className="formfield" placeholder="Bunmi" />
						</div>
						<div className="input-box">
							<div className="input-label">Last Name</div>
							<input type="text" className="formfield" placeholder="Omotosho" />
						</div>
						<div className="input-box">
							<div className="input-label">Mobile Number</div>
							<input
								type="text"
								className="formfield"
								placeholder="080 1234 5678"
							/>
						</div>
						<div className="input-box">
							<div className="input-label">Email Address</div>
							<input
								type="text"
								className="formfield"
								placeholder="bomotosho@yahoo.com"
							/>
						</div>
						<div className="input-box">
							<div className="input-label">Residential Address</div>
							<input
								type="text"
								className="formfield"
								placeholder="10 Breadfruit Street, Lagos Island, Lagos Nigeria"
							/>
						</div>
						<div className="input-box">
							<div className="input-label">Relationship</div>
							<input type="text" className="formfield" placeholder="Wife" />
						</div>
					</div>
				</div>
				<button className="secondary-btn">Ok</button>
			</div>
		</div>
	);
}
