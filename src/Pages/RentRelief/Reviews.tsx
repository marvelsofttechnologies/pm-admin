import React from "react";

export default function Reviews() {
	return (
		<div className="inner-wrapper d-none">
			<div className="flex-item mb-4">
				<div className="admin-search-bar-2">
					<i className="fas fa-search" />
					<input type="text" className="property-search" placeholder="Search" />
				</div>
				<div className="flex">
					<button className="ex-btn">
						<span>Export</span>
						<i className="far fa-file-export" />
					</button>
					<div className="input-box">
						<div className="ns">
							<select>
								<option>Filter</option>
							</select>
							<div className="arrow" />
						</div>
					</div>
				</div>
			</div>
			<div className="table-box">
				<table className="table table-hover">
					<thead className="ag-thead">
						<tr>
							<th scope="col">User</th>
							<th scope="col">Amount</th>
							<th scope="col">Term</th>
							<th scope="col">Type</th>
							<th scope="col">Schedule</th>
							<th scope="col">Time</th>
							<th scope="col">Status</th>
							<th scope="col" />
						</tr>
					</thead>
					<tbody>
						<tr>
							<th scope="row">
								<div className="user-pic mr-1">PO</div>
								Pade Omotosho
							</th>
							<td>N2,500,000</td>
							<td className="user">6 Months</td>
							<td className="user">RentRelief</td>
							<td>Monthly</td>
							<td>1D 21H (2/02/21)</td>
							<td>Pending</td>
							<td>
								<div className="flex">
									<div className="del-icon">
										<i className="fas fa-times" />
									</div>
									<div className="accpt-icon">
										<i className="fas fa-arrow-right" />
									</div>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div className="pm-space" />
			<div className="pagination">
				<div className="page-arrows">
					<i className="fal fa-angle-left" />
				</div>
				<div className="page-number">1</div>
				<div className="page-number nb">of 238</div>
				<div className="page-arrows">
					<i className="fal fa-angle-right" />
				</div>
			</div>
		</div>
	);
}
