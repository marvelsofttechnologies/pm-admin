import React from "react";

export default function Sourcing() {
	return (
		<div className="inner-wrapper">
			<div className="flex">
				<div className="sourcing">
					<div className="bank-icon" />
					<div className="source-info">
						<p>Providus Bank</p>
						<h3>₦136,670,342.78</h3>
					</div>
				</div>
				<div className="sourcing">
					<div className="bank-icon" />
					<div className="source-info">
						<p>Total Loan Requests</p>
						<h3>₦120,670,342.78</h3>
					</div>
					<button className="approve-btn">Send all for Approval</button>
				</div>
			</div>
		</div>
	);
}
