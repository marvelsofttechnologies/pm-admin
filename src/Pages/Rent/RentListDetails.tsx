import React from "react";

export default function RentListDetails() {
	return (
		<>
			{/* Application Rent  */}
			<div className="flex d-none">
				<div className="applicant-details">
					<div className="flex pt-3">
						<div className="back-icon">
							<i className="fas fa-chevron-left" />
						</div>
						<div className="listing-full">Semi-detached Duplex</div>
					</div>
					<div className="list-tip">Applicant's Details</div>
					<div className="row">
						<div className="col-lg-6">
							<div className="pic-label">Passport Photograph</div>
							<div className="pic-box" />
							<div className="per-details">
								<label className="tab-label">Application Date</label>
								<span className="tab-text">22/06/21</span>
							</div>
							<div className="per-details">
								<label className="tab-label">First Name</label>
								<span className="tab-text">Pade</span>
							</div>
							<div className="per-details">
								<label className="tab-label">Middle Name</label>
								<span className="tab-text">Craig</span>
							</div>
							<div className="per-details">
								<label className="tab-label">Surame</label>
								<span className="tab-text">Omotosho</span>
							</div>
							<div className="per-details">
								<label className="tab-label">Mobile Number</label>
								<span className="tab-text">080 1234 5678</span>
							</div>
							<div className="per-details">
								<label className="tab-label">Email</label>
								<span className="tab-text">padeomotoso@gmail.com</span>
							</div>
							<div className="per-details">
								<label className="tab-label">Current Residential Address</label>
								<span className="tab-text">
									20 Adedoyin Oludaree Street Omole Phase 4, Ikeja, Lagos
								</span>
							</div>
							<div className="per-details">
								<label className="tab-label">Date of Birth</label>
								<span className="tab-text">13/04/80</span>
							</div>
							<div className="per-details">
								<label className="tab-label">Nationality</label>
								<span className="tab-text">Nigerian</span>
							</div>
							<div className="per-details">
								<label className="tab-label">Marital Status</label>
								<span className="tab-text">Married</span>
							</div>
							<div className="per-details">
								<label className="tab-label">Occupation</label>
								<span className="tab-text">Lawyer</span>
							</div>
							<div className="per-details">
								<label className="tab-label">Employer</label>
								<span className="tab-text">Banwo &amp; Ighadola</span>
							</div>
						</div>
						<div className="col-lg-6">
							<div className="pic-label">Passport Photograph</div>
							<div className="id-box">
								<img src="" alt="" />
							</div>
							<div className="per-details">
								<label className="tab-label">Work Address</label>
								<span className="tab-text">33D Cameron Road, Ikoyi, Lagos</span>
							</div>
							<div className="per-details">
								<label className="tab-label">Annual Income</label>
								<span className="tab-text">₦13,670,000</span>
							</div>
							<div className="list-tip">Next of Kin</div>
							<div className="per-details">
								<label className="tab-label">First Name</label>
								<span className="tab-text">Bunmi</span>
							</div>
							<div className="per-details">
								<label className="tab-label">Surname</label>
								<span className="tab-text">Omotosho</span>
							</div>
							<div className="per-details">
								<label className="tab-label">Relationship</label>
								<span className="tab-text">Spouse</span>
							</div>
							<div className="per-details">
								<label className="tab-label">Mobile Number</label>
								<span className="tab-text">080 1234 5678</span>
							</div>
							<div className="per-details">
								<label className="tab-label">Email</label>
								<span className="tab-text">padeomotoso@gmail.com</span>
							</div>
							<div className="per-details">
								<label className="tab-label">Address</label>
								<span className="tab-text">
									20 Adedoyin Oludaree Street Omole Phase 4, Ikeja, Lagos
								</span>
							</div>
							<div className="per-details">
								<label className="tab-label">Location</label>
								<span className="tab-text">Omole Phase 1 , Lagos</span>
							</div>
						</div>
					</div>
				</div>
				<div className="owner-details">
					<div className="flex justify-content-end">
						<div className="btn-outline">View Property</div>
					</div>
					<div className="row">
						<div className="col-lg-6">
							<div className="list-tip">Owner's Details</div>
							<div className="per-details">
								<label className="tab-label">Name</label>
								<span className="tab-text">Kamoru Hopewell</span>
							</div>
							<div className="per-details">
								<label className="tab-label">Email</label>
								<span className="tab-text">kamoruhopewell@gmail.com</span>
							</div>
							<div className="per-details">
								<label className="tab-label">Mobile Number</label>
								<span className="tab-text">09876543211</span>
							</div>
							<div className="list-tip">Owner’s Requirements</div>
							<div className="per-details">
								<label className="tab-label">Management</label>
								<span className="tab-text">Self</span>
							</div>
							<div className="per-details">
								<label className="tab-label">Tenant Type</label>
								<span className="tab-text">Family/Individual</span>
							</div>
							<div className="per-details">
								<label className="tab-label">Income Bracket</label>
								<span className="tab-text">₦500,000 - ₦750,000</span>
							</div>
							<div className="per-details">
								<label className="tab-label">
									Frequency of Rent Collection
								</label>
								<span className="tab-text">Annually</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	);
}
