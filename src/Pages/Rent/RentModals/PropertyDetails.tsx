import React, { useState } from "react";
import { SRLWrapper } from "simple-react-lightbox";
import { PropertyView } from "../../../Utilities/Types";
import parse from "html-react-parser";
//@ts-ignore
import Naira from "react-naira";
import { AllListings } from "../../../Classes/AllListings";
import { useToasts } from "react-toast-notifications";
import { useHistory } from "react-router";
import Modal from "../../../Utilities/Modal/Modal";
import MiniModal from "../../../Utilities/Modal/MiniModal";
import Spinner from "../../../Utilities/Spinner";
import { FaHouseDamage } from "react-icons/fa"
import { CgFileDocument } from "react-icons/cg"
import Complaints from "./Complaints";
import { Statuses } from "../../../Utilities/Enums";

export default function PropertyDetails({
  property,
}: {
  property?: PropertyView;
}) {
  const [loading, setLoading] = useState(false);
  const { addToast } = useToasts();
  const [openComplaint, setOpenComplaint] = useState(false);
  const [deleteModal, setDeleteModal] = useState(false);
  const [propertyId, setPropertyId] = useState(0);
  let history = useHistory();
  
  const deleteProperty = async (propertyId: number) => {
    setLoading(true);
    try {
      let data = await AllListings.Delete(propertyId);
      if (data.status) {
        history.push("/listings");
        setLoading(false);
        addToast("Property Successfully Deleted", {
          appearance: "success",
          autoDismiss: true,
        });
        console.log(data.message);

        return;
      }
      addToast(data.message, { appearance: "error", autoDismiss: true });
    } catch (error) {
      console.error(error);
      setLoading(false);
    }
  };
  
  const openComplaintModal = () => {
    setOpenComplaint(!openComplaint);
  }
  
  
  return (
    <>
      <MiniModal
        open={deleteModal}
        onClose={() => {
          setDeleteModal(false);
        }}
        height={"150px"}
        width={"30%"}
      >
        <div className="flex-column justify-content-center">
          <h5 className="text-center">
            Are you sure you want to delete <span>{property?.name} </span>
          </h5>
          <div className="row  flex-center justify-space-between mt-3">
            <button
              className="user-red-btn-a "
              style={{ backgroundColor: "#000", marginRight: "1rem" }}
              onClick={async () => await setDeleteModal(false)}
            >
              No
            </button>

            <button
              className="user-red-btn-a "
              onClick={async () => await deleteProperty(propertyId)}
            >
              {loading ? <Spinner /> : "Yes"}
            </button>
          </div>
        </div>
      </MiniModal>
      
      <Modal open={openComplaint} onClose={() => setOpenComplaint(false)}>
        <Complaints close={() => setOpenComplaint(false)} />
      </Modal>

      <div className="single-application-details mr-1">
        <div className="listing-show">
          <div className="list-tip my-0">Property</div>
          <div className="property-info-box w-100">
            
            <div className="owner-details-box">
                <div className="per-details">
                    <label className="tab-label">Annual Rent</label>
                    <span className="tab-text"><Naira>40000000</Naira></span>
                </div>
                
                <div className="mt-2">
                    <button className="d-flex align-items-center document-wrap w-100 my-3 py-2 px-3" onClick={() => openComplaintModal()}>
						<div className="icon"> <FaHouseDamage /> </div>
						<div className="item ml-3">
							<p className="mb-0">Complaint</p>
						</div>
					</button>
                    <button className="d-flex align-items-center document-wrap w-100 my-3 py-2 px-3" onClick={() => {}}>
						<div className="icon"> <CgFileDocument /> </div>
						<div className="item ml-3">
							<p className="mb-0">Tenancy Agreement</p>
						</div>
					</button>
                </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
