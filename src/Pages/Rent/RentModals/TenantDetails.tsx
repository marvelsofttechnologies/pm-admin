import React, { useState } from "react";
import { SRLWrapper } from "simple-react-lightbox";
import { PropertyView } from "../../../Utilities/Types";
import parse from "html-react-parser";
//@ts-ignore
import Naira from "react-naira";
import { AllListings } from "../../../Classes/AllListings";
import { useToasts } from "react-toast-notifications";
import { useHistory } from "react-router";
import MiniModal from "../../../Utilities/Modal/MiniModal";
import Spinner from "../../../Utilities/Spinner";
import { Statuses } from "../../../Utilities/Enums";

export default function TenantDetails({
  property,
}: {
  property?: PropertyView;
}) {
  const [loading, setLoading] = useState(false);
  const { addToast } = useToasts();
  const [approving, setApproving] = useState(false);
  const [rejecting, setRejecting] = useState(false);
  const [deleteModal, setDeleteModal] = useState(false);
  const [propertyId, setPropertyId] = useState(0);
  let history = useHistory();

  return (
    <>
      <div className="single-application-details mr-1">
        <div className="listing-show">
          <div className="list-tip my-0">Tenant</div>
          <div className="property-info-box w-100">
            
            {   property?.mediaFiles && property?.mediaFiles?.length > 0
                ? property?.mediaFiles.map((file, index) => {
                    return file.isImage ? (
                      <div className="img-trash-wrapper">
                        {/* <i
                          className="fas fa-trash img-trash"
                          onClick={async () => await deleteMedia(file.id)}
                        ></i> */}
                        <img
                          src={file.url}
                          alt=""
                          className="admin-img-field"
                        />
                      </div>
                    ) : null;
                  })
                : null
            }
            <div className="img-trash-wrapper">
              {/* <i
                className="fas fa-trash img-trash"
                onClick={async () => await deleteMedia(file.id)}
              ></i> */}
              <img
                src="https://www.pixsy.com/wp-content/uploads/2021/04/ben-sweet-2LowviVHZ-E-unsplash-1.jpeg"
                alt=""
                className="admin-img-field"
              />
            </div>
            
            <div className="owner-details-box">
                <div className="per-details">
                    <label className="tab-label">First Name</label>
                    <span className="tab-text">Adelowo</span>
                </div>
                <div className="per-details">
                    <label className="tab-label">Middle Name</label>
                    <span className="tab-text">Adelowo</span>
                </div>
                <div className="per-details">
                    <label className="tab-label">Surname</label>
                    <span className="tab-text">Adelowo</span>
                </div>
                <div className="per-details">
                    <label className="tab-label">Mobile Number</label>
                    <span className="tab-text">Adelowo</span>
                </div>
                <div className="per-details">
                    <label className="tab-label">Email</label>
                    <span className="tab-text">Adelowo@gmail.com</span>
                </div>
                
                <div className="my-5">
                    <button className="admin-action mx-0 w-100">See Tenant's Details</button>
                </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
