import MaterialModal from "@material-ui/core/Modal";
import React from "react";
import { ModalWrapper } from "./ModalWrapper";

function Modal(props: any) {
	return (
		<MaterialModal
			open={props.open}
			onClose={props.onClose}
			aria-labelledby="This is a modal"
			aria-describedby="A modal description"
			disableBackdropClick={false}
		>
			<ModalWrapper style={{ height: props.height, width: props.width }}>
				{props.children}
			</ModalWrapper>
		</MaterialModal>
	);
}

export default Modal;
