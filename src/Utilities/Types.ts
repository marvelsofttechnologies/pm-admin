export interface IRoutes {
  path: string;
  key: string;
  exact: boolean;
  component:
    | React.ComponentType<React.ComponentProps<any>>
    | React.ComponentType<any>;
  routes?: IRoutes[];
}

export interface LoginModel {
  email: string;
  password: string;
}
export interface UserView {
  id?: number;
  email?: string;
  fullName?: string;
  firstName?: string;
  lastName?: string;
  token?: string;
  phoneNumber?: string;
  medias?: MediaView[];
  properties: PropertyView[];
  bank?: string;
  accountNumber?: string;
}
export interface ContextData {
  user?: UserView;
}
export interface Bank {
  bankName: string;
  bankAccountNumber: number;
}

export interface UserInput {
  email?: string;
  password?: string;
  firstName?: string;
  lastName?: string;
  phoneNumber?: string;
  usersName?: string;
  country?: string;
  accountNumber?: string;
  accountName?: string;
  bankName?: string;
  reffererCode?: string;
  media?: MediaInput[];
  description?: string;
  officeAddress?: string;
}

export interface MediaInput {
  name?: string;
  extention?: string;
  base64String?: string;
  propertyId?: number;
  isImage?: boolean;
  isVideo?: boolean;
  isDocument?: boolean;
}
export interface Response {
  status: boolean;
  message: string;
  data: any;
}

export interface ApiResponse {
  status: any;
  message: string;
  data: any;
  error: any[];
  relations: any;
  href: string;
  method: string;
  routeName: string;
  routeValues: any;
}

export interface ApiLink {
  href: string;
  rel: string[];
}

export interface PagedApiResponse extends ApiResponse {
  data: PagedApiResponseData;
}

export interface PagedApiResponseData {
  offset?: string;
  limit?: string;
  size?: number;
  value?: any[];
  first?: ApiLink;
  last?: ApiLink;
  next?: ApiLink;
  self?: ApiLink;
  previous?: ApiLink;
}
export interface PropertyType {
  id?: number;
  dateCreated?: Date;
  dateModified?: Date;
  name?: string;
}
export interface MediaView {
  id: number;
  dateCreated: Date;
  dateModified: Date;
  name: string;
  extention: string;
  url: string;
  base64String: string;
  propertyId: number;
  isImage: boolean;
  isVideo: boolean;
  isDocument: boolean;
}

export interface PropertyView {
  id?: number;
  dateCreated: Date;
  dateModified?: Date;
  name?: string;
  title?: string;
  address?: string;
  description?: string;
  state?: string;
  lga?: string;
  sellMyself?: boolean;
  price?: number;
  numberOfBedrooms?: number;
  numberOfBathrooms?: number;
  isDraft?: boolean;
  isActive?: boolean;
  isForRent?: boolean;
  isForSale?: boolean;
  area?: string;
  verified?: boolean;
  representative?: UserView;
  representativeId?: number;
  propertyType?: PropertyType;
  propertyTypeId?: number;
  createdByUser?: UserView;
  mediaFiles?: MediaView[];
  views?: number;
  enquiries?: number;
  longitude?: number;
  latitude?: number;
  isRequest?: boolean;
  status?: string;
  budget?: string;
  property: PropertyView;
  user?: UserView;
}
export interface PropertyInput {
  id?: number;
  name?: string;
  title?: string;
  address?: string;
  description?: string;
  sellMyself?: boolean;
  price?: number;
  numberOfBedrooms?: number;
  numberOfBathrooms?: number;
  isDraft?: boolean;
  isActive?: boolean;
  isForRent?: boolean;
  isForSale?: boolean;
  propertyTypeId?: number;
  mediaFiles?: MediaInput[];
  state?: string;
  lga?: string;
  area?: string;
  isRequest?: boolean;
  comment?: string;
  budget?: string;
  statusId?: number;
}
export interface Metrics {
  newUsers: string;
  activeUsers: string;
  users: string;
}

export interface TransactionView {
  userId?: number;
  user?: RentReliefUser;
  propertyId?: number;
  rentReliefId?: number;
  rentRelief?: TransactionViewRentRelief;
  property?: TransactionViewProperty;
  transactionReference?: string;
  paymentLogId?: number;
  paymentLog?: TransactionViewPaymentLog;
  status?: string;
  string?: string;
  description?: string;
  title?: string;
  amount?: string;
  dateCreated?:       Date;
}

export interface TransactionViewPaymentLog {
  status?: string;
  property?: Property;
  user?: RepresentativeClass;
  transaction?: Transaction;
}

export interface PaymentLogProperty {
  id?: number;
  dateCreated?: Date;
  dateModified?: Date;
  name?: string;
  title?: string;
  address?: string;
  description?: string;
  state?: string;
  lga?: string;
  sellMyself?: boolean;
  price?: number;
  numberOfBedrooms?: number;
  numberOfBathrooms?: number;
  isDraft?: boolean;
  isActive?: boolean;
  isForRent?: boolean;
  isForSale?: boolean;
  area?: string;
  verified?: boolean;
  representativeId?: number;
  propertyTypeId?: number;
  propertyType?: PropertyType;
  createdByUserId?: number;
  mediaFiles?: RepresentativePassportPhotograph[];
  isRequest?: boolean;
  statusId?: number;
  status?: PropertyType;
  reports?: Report[];
  views?: number;
  enquiries?: number;
  longitude?: number;
  latitude?: number;
  userEnquiries?: Report[];
  tenantTypeId?: number;
  tenantType?: PropertyType;
  rentCollectionType?: PropertyType;
  rentCollectionTypeId?: number;
  inspections?: Inspection[];
}

export interface Inspection {
  id?: number;
  dateCreated?: Date;
  dateModified?: Date;
  inspectionDateId?: number;
  inspectionTimeId?: number;
  inspectionDate?: InspectionDate;
  inspectionTime?: Time;
  userId?: number;
  propertyId?: number;
}

export interface InspectionDate {
  id?: number;
  dateCreated?: Date;
  dateModified?: Date;
  propertyId?: number;
  date?: Date;
  times?: Time[];
  inspections?: null[];
}

export interface Time {
  id?: number;
  dateCreated?: Date;
  dateModified?: Date;
  inspectionDateId?: number;
  availableTime?: Date;
  isAvailable?: boolean;
}

export interface RepresentativePassportPhotograph {
  id?: number;
  dateCreated?: Date;
  dateModified?: Date;
  name?: string;
  extention?: string;
  url?: string;
  base64String?: string;
  propertyId?: number;
  isImage?: boolean;
  isVideo?: boolean;
  isDocument?: boolean;
}

export interface PropertyType {
  id?: number;
  dateCreated?: Date;
  dateModified?: Date;
  name?: string;
}

export interface Report {
  id?: number;
  dateCreated?: Date;
  dateModified?: Date;
  propertyId?: number;
  userId?: number;
  description?: number;
  active?: boolean;
}

export interface Transaction {
  id?: number;
  dateCreated?: Date;
  dateModified?: Date;
  userId?: number;
  propertyId?: number;
  rentReliefId?: number;
  rentRelief?: RentReliefElement;
  property?: Property;
  transactionReference?: string;
  paymentLogId?: number;
  paymentLog?: TransactionPaymentLog;
  statusId?: number;
  status?: PropertyType;
  string?: string;
  description?: string;
  title?: string;
}

export interface TransactionPaymentLog {
  id?: number;
  dateCreated?: Date;
  dateModified?: Date;
  flutterWavePaymentId?: number;
  transactionReference?: string;
  flutterWaveReference?: string;
  deviceFingerPrint?: string;
  string?: string;
  currency?: string;
  chargedstring?: string;
  appFee?: string;
  merchantFee?: string;
  processorResponse?: string;
  authModel?: string;
  ip?: string;
  narration?: string;
  status?: string;
  paymentType?: string;
  accountId?: number;
  stringSettled?: number;
  createdAt?: Date;
  cardId?: number;
  card?: Card;
}

export interface Card {
  id?: number;
  dateCreated?: Date;
  dateModified?: Date;
  first6Digits?: string;
  last4Digits?: string;
  issuer?: string;
  country?: string;
  type?: string;
  token?: string;
  expiry?: string;
}

export interface RentReliefElement {
  id?: number;
  dateCreated?: Date;
  dateModified?: Date;
  propertyId?: number;
  property?: Property;
  userId?: number;
  installments?: PurpleInstallment[];
}

export interface PurpleInstallment {
  id?: number;
  dateCreated?: Date;
  dateModified?: Date;
  string?: number;
  dateDue?: Date;
  statusId?: number;
  status?: PropertyType;
  rentReliefId?: number;
}

export interface RepresentativeClass {
  id?: number;
  userName?: string;
  normalizedUserName?: string;
  email?: string;
  normalizedEmail?: string;
  emailConfirmed?: boolean;
  passwordHash?: string;
  securityStamp?: string;
  concurrencyStamp?: string;
  phoneNumber?: string;
  phoneNumberConfirmed?: boolean;
  twoFactorEnabled?: boolean;
  lockoutEnd?: Date;
  lockoutEnabled?: boolean;
  accessFailedCount?: number;
  firstName?: string;
  lastName?: string;
  password?: string;
  token?: string;
  dateCreated?: Date;
  dateModified?: Date;
  codes?: Code[];
  reports?: Report[];
  applications?: Application[];
  middleName?: string;
  address?: string;
  dateOfBirth?: Date;
  nationality?: string;
  maritalStatus?: string;
  employer?: string;
  occupation?: string;
  workAddress?: string;
  annualIncome?: string;
  passportPhotographId?: number;
  passportPhotograph?: RepresentativePassportPhotograph;
  workIdId?: number;
  workId?: RepresentativePassportPhotograph;
  transactions?: Transaction[];
  isAdmin?: boolean;
  properties?: Property[];
  userEnquiries?: Report[];
  inspections?: Inspection[];
  cleanings?: Cleaning[];
  rentReliefs?: RentReliefElement[];
}

export interface Application {
  id?: number;
  dateCreated?: Date;
  dateModified?: Date;
  userId?: number;
  propertyId?: number;
  property?: Property;
  nextOfKinId?: number;
  nextOfKin?: NextOfKin;
  applicationTypeId?: number;
  applicationType?: PropertyType;
  statusId?: number;
  status?: PropertyType;
  reliefstring?: number;
  payBackDate?: Date;
  repaymentFrequency?: string;
}

export interface NextOfKin {
  id?: number;
  dateCreated?: Date;
  dateModified?: Date;
  firstName?: string;
  lastName?: string;
  middleName?: string;
  email?: string;
  phoneNumber?: string;
  relationship?: string;
  address?: string;
}

export interface Cleaning {
  id?: number;
  dateCreated?: Date;
  dateModified?: Date;
  buildingState?: string;
  propertyTypeId?: number;
  dateNeeded?: Date;
  numberOfBathrooms?: number;
  numberOfBedrooms?: number;
  numberOfFloors?: number;
  buildingType?: string;
  userId?: number;
  statusId?: number;
  status?: PropertyType;
  cleaningQuoteId?: number;
  cleaningQuote?: CleaningQuote;
}

export interface CleaningQuote {
  id?: number;
  dateCreated?: Date;
  dateModified?: Date;
  quote?: number;
  proposedDate?: Date;
  statusId?: number;
  status?: PropertyType;
}

export interface Code {
  id?: number;
  dateCreated?: Date;
  dateModified?: Date;
  codeString?: string;
  key?: string;
  userId?: number;
  expiryDate?: Date;
  isExpired?: boolean;
  token?: string;
}

export interface TransactionViewProperty {
  id?: number;
  name?: string;
  title?: string;
  address?: string;
  description?: string;
  sellMyself?: boolean;
  price?: number;
  numberOfBedrooms?: number;
  numberOfBathrooms?: number;
  isDraft?: boolean;
  isActive?: boolean;
  isForRent?: boolean;
  isForSale?: boolean;
  area?: string;
  propertyType?: string;
  mediaFiles?: PurplePassportPhotograph[];
  verified?: boolean;
  representative?: RepresentativeClass;
  state?: string;
  lga?: string;
  isRequest?: boolean;
  status?: string;
  longitude?: number;
  latitude?: number;
  views?: number;
  enquiries?: number;
  dateCreated?: Date;
}

export interface PurplePassportPhotograph {
  url?: string;
  isImage?: boolean;
  isVideo?: boolean;
  isDocument?: boolean;
}

export interface TransactionViewRentRelief {
  propertyId?: number;
  property?: Property;
  user?: RentReliefUser;
  installments?: FluffyInstallment[];
}

export interface FluffyInstallment {
  string?: number;
  dateDue?: Date;
  status?: string;
}

export interface RentReliefUser {
  id?: number;
  email?: string;
  fullName?: string;
  firstName?: string;
  lastName?: string;
  token?: string;
  phoneNumber?: string;
  properties?: TransactionViewProperty[];
  passportPhotograph?: PurplePassportPhotograph;
  workId?: PurplePassportPhotograph;
  annualIncome?: string;
  maritalStatus?: string;
  occupation?: string;
  nationality?: string;
  dateOfBirth?: Date;
  address?: string;
}

export interface CleanView {
  buildingState?:     string;
  propertyTypeId?:    number;
  dateNeeded?:        Date;
  numberOfBathrooms?: number;
  numberOfBedrooms?:  number;
  numberOfFloors?:    number;
  buildingType?:      string;
  fileName?:          string;
  fileNumber?:        string;
  status?:            string;
  dateCreated?:       Date;
  propertyType?: string;
  user?: UserView;
  id?: number;
}

export interface Transaction {
  id?:                   number;
  dateCreated?:          Date;
  dateModified?:         Date;
  userId?:               number;
  propertyId?:           number;
  rentReliefId?:         number;
  rentRelief?:           RentReliefElement;
  property?:             Property;
  transactionReference?: string;
  paymentLogId?:         number;
  paymentLog?:           TransactionPaymentLog;
  statusId?:             number;
  status?:               PropertyType;
  amount?:               string;
  description?:          string;
  title?:                string;
}

export interface User {
  id?:                   number;
  userName?:             string;
  normalizedUserName?:   string;
  email?:                string;
  normalizedEmail?:      string;
  emailConfirmed?:       boolean;
  passwordHash?:         string;
  securityStamp?:        string;
  concurrencyStamp?:     string;
  phoneNumber?:          string;
  phoneNumberConfirmed?: boolean;
  twoFactorEnabled?:     boolean;
  lockoutEnd?:           Date;
  lockoutEnabled?:       boolean;
  accessFailedCount?:    number;
  firstName?:            string;
  lastName?:             string;
  password?:             string;
  token?:                string;
  dateCreated?:          Date;
  dateModified?:         Date;
  codes?:                Code[];
  reports?:              Report[];
  applications?:         Application[];
  middleName?:           string;
  address?:              string;
  dateOfBirth?:          Date;
  nationality?:          string;
  maritalStatus?:        string;
  employer?:             string;
  occupation?:           string;
  workAddress?:          string;
  annualIncome?:         string;
  passportPhotographId?: number;
  passportPhotograph?:   PassportPhotograph;
  workIdId?:             number;
  workId?:               PassportPhotograph;
  transactions?:         Transaction[];
  isAdmin?:              boolean;
  properties?:           Property[];
  userEnquiries?:        null[];
  inspections?:          Inspection[];
  cleanings?:            Cleaning[];
  rentReliefs?:          UserEnquiries[];
}

export interface UserEnquiries {
  id?:           number;
  dateCreated?:  Date;
  dateModified?: Date;
  userId?:       number;
  user?:         User;
  propertyId?:   number;
  property?:     Property;
  active?:       boolean;
  installments?: Installment[];
}

export enum Address {
  String = "string",
}

export interface PaymentLog {
  id?:                   number;
  dateCreated?:          Date;
  dateModified?:         Date;
  flutterWavePaymentId?: number;
  transactionReference?: string;
  flutterWaveReference?: string;
  deviceFingerPrint?:    string;
  amount?:               string;
  currency?:             string;
  chargedAmount?:        string;
  appFee?:               string;
  merchantFee?:          string;
  processorResponse?:    string;
  authModel?:            string;
  ip?:                   string;
  narration?:            string;
  status?:               string;
  paymentType?:          string;
  accountId?:            number;
  amountSettled?:        number;
  createdAt?:            Date;
  cardId?:               number;
  card?:                 Card;
}

export interface Card {
  id?:           number;
  dateCreated?:  Date;
  dateModified?: Date;
  first6Digits?: string;
  last4Digits?:  string;
  issuer?:       string;
  country?:      string;
  type?:         string;
  token?:        string;
  expiry?:       string;
}

export interface Property {
  id?:                   number;
  dateCreated?:          Date;
  dateModified?:         Date;
  name?:                 string;
  title?:                string;
  address?:              string;
  description?:          string;
  state?:                string;
  lga?:                  string;
  sellMyself?:           boolean;
  price?:                number;
  numberOfBedrooms?:     number;
  numberOfBathrooms?:    number;
  isDraft?:              boolean;
  isActive?:             boolean;
  isForRent?:            boolean;
  isForSale?:            boolean;
  area?:                 string;
  verified?:             boolean;
  representativeId?:     number;
  propertyTypeId?:       number;
  propertyType?:         PropertyType;
  createdByUserId?:      number;
  mediaFiles?:           PassportPhotograph[];
  isRequest?:            boolean;
  statusId?:             number;
  status?:               PropertyType;
  reports?:              Report[];
  views?:                number;
  enquiries?:            number;
  longitude?:            number;
  latitude?:             number;
  userEnquiries?:        null[];
  tenantTypeId?:         number;
  tenantType?:           PropertyType;
  rentCollectionType?:   PropertyType;
  rentCollectionTypeId?: number;
  inspections?:          Inspection[];
}

export interface Inspection {
  id?:               number;
  dateCreated?:      Date;
  dateModified?:     Date;
  inspectionDateId?: number;
  inspectionTimeId?: number;
  inspectionDate?:   InspectionDate;
  inspectionTime?:   Time;
  userId?:           number;
  propertyId?:       number;
}

export interface InspectionDate {
  id?:           number;
  dateCreated?:  Date;
  dateModified?: Date;
  propertyId?:   number;
  date?:         Date;
  times?:        Time[];
  inspections?:  null[];
}

export interface Time {
  id?:               number;
  dateCreated?:      Date;
  dateModified?:     Date;
  inspectionDateId?: number;
  availableTime?:    Date;
  isAvailable?:      boolean;
}

export interface PassportPhotograph {
  id?:           number;
  dateCreated?:  Date;
  dateModified?: Date;
  name?:         string;
  extention?:    string;
  url?:          string;
  base64String?: string;
  propertyId?:   number;
  isImage?:      boolean;
  isVideo?:      boolean;
  isDocument?:   boolean;
}

export interface PropertyType {
  id?:           number;
  dateCreated?:  Date;
  dateModified?: Date;
  name?:         string;
}

export interface Report {
  id?:           number;
  dateCreated?:  Date;
  dateModified?: Date;
  propertyId?:   number;
  userId?:       number;
  description?:  number;
}

export interface Application {
  id?:                 number;
  dateCreated?:        Date;
  dateModified?:       Date;
  userId?:             number;
  propertyId?:         number;
  property?:           Property;
  nextOfKinId?:        number;
  nextOfKin?:          NextOfKin;
  applicationTypeId?:  number;
  applicationType?:    PropertyType;
  statusId?:           number;
  status?:             PropertyType;
  reliefAmount?:       number;
  payBackDate?:        Date;
  repaymentFrequency?: string;
}

export interface NextOfKin {
  id?:           number;
  dateCreated?:  Date;
  dateModified?: Date;
  firstName?:    string;
  lastName?:     string;
  middleName?:   string;
  email?:        string;
  phoneNumber?:  string;
  relationship?: string;
  address?:      string;
}

export interface Cleaning {
  id?:                number;
  dateCreated?:       Date;
  dateModified?:      Date;
  buildingState?:     string;
  propertyTypeId?:    number;
  propertyType?:      PropertyType;
  dateNeeded?:        Date;
  numberOfBathrooms?: number;
  numberOfBedrooms?:  number;
  numberOfFloors?:    number;
  buildingType?:      string;
  userId?:            number;
  statusId?:          number;
  status?:            PropertyType;
  cleaningQuotes?:    CleaningQuote[];
}

export interface CleaningQuote {
  id?:           number;
  dateCreated?:  Date;
  dateModified?: Date;
  quote?:        number;
  proposedDate?: Date;
  statusId?:     number;
  status?:       PropertyType;
  cleaningId?:   number;
}

export interface Code {
  id?:           number;
  dateCreated?:  Date;
  dateModified?: Date;
  codeString?:   string;
  key?:          string;
  userId?:       number;
  expiryDate?:   Date;
  isExpired?:    boolean;
  token?:        string;
}

export interface Installment {
  id?:           number;
  dateCreated?:  Date;
  dateModified?: Date;
  amount?:       number;
  dateDue?:      Date;
  statusId?:     number;
  status?:       PropertyType;
  rentReliefId?: number;
}

