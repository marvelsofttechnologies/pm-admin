export interface Transaction {
    id?:                   number;
    dateCreated?:          Date;
    dateModified?:         Date;
    userId?:               number;
    propertyId?:           number;
    rentReliefId?:         number;
    rentRelief?:           ApplicationView;
    property?:             Property;
    transactionReference?: string;
    paymentLogId?:         number;
    paymentLog?:           PaymentLog;
    statusId?:             number;
    status?:               PropertyType;
    amount?:               string;
    description?:          string;
    title?:                string;
}

export interface User {
    id?:                   number;
    userName?:             string;
    normalizedUserName?:   string;
    email?:                string;
    normalizedEmail?:      string;
    emailConfirmed?:       boolean;
    passwordHash?:         string;
    securityStamp?:        string;
    concurrencyStamp?:     string;
    phoneNumber?:          string;
    phoneNumberConfirmed?: boolean;
    twoFactorEnabled?:     boolean;
    lockoutEnd?:           Date;
    lockoutEnabled?:       boolean;
    accessFailedCount?:    number;
    firstName?:            string;
    lastName?:             string;
    password?:             string;
    token?:                string;
    dateCreated?:          Date;
    dateModified?:         Date;
    codes?:                Code[];
    reports?:              Report[];
    applications?:         Application[];
    middleName?:           string;
    address?:              string;
    dateOfBirth?:          Date;
    nationality?:          string;
    maritalStatus?:        string;
    employer?:             string;
    occupation?:           string;
    workAddress?:          string;
    annualIncome?:         string;
    passportPhotographId?: number;
    passportPhotograph?:   PassportPhotograph;
    workIdId?:             number;
    workId?:               PassportPhotograph;
    transactions?:         Transaction[];
    isAdmin?:              boolean;
    properties?:           Property[];
    userEnquiries?:        null[];
    inspections?:          Inspection[];
    cleanings?:            Cleaning[];
    rentReliefs?:          ApplicationView[];
}

export interface ApplicationView {
    id?:           number;
    dateCreated?:  Date;
    dateModified?: Date;
    userId?:       number;
    user?:         User;
    propertyId?:   number;
    property?:     Property;
    active?:       boolean;
    installments?: Installment[];
    reliefAmount?:       number;
}


export interface PaymentLog {
    id?:                   number;
    dateCreated?:          Date;
    dateModified?:         Date;
    flutterWavePaymentId?: number;
    transactionReference?: string;
    flutterWaveReference?: string;
    deviceFingerPrint?:    string;
    amount?:               string;
    currency?:             string;
    chargedAmount?:        string;
    appFee?:               string;
    merchantFee?:          string;
    processorResponse?:    string;
    authModel?:            string;
    ip?:                   string;
    narration?:            string;
    status?:               string;
    paymentType?:          string;
    accountId?:            number;
    amountSettled?:        number;
    createdAt?:            Date;
    cardId?:               number;
    card?:                 Card;
}

export interface Card {
    id?:           number;
    dateCreated?:  Date;
    dateModified?: Date;
    first6Digits?: string;
    last4Digits?:  string;
    issuer?:       string;
    country?:      string;
    type?:         string;
    token?:        string;
    expiry?:       string;
}

export interface Property {
    id?:                   number;
    dateCreated?:          Date;
    dateModified?:         Date;
    name?:                 string;
    title?:                string;
    address?:              string;
    description?:          string;
    state?:                string;
    lga?:                  string;
    sellMyself?:           boolean;
    price?:                number;
    numberOfBedrooms?:     number;
    numberOfBathrooms?:    number;
    isDraft?:              boolean;
    isActive?:             boolean;
    isForRent?:            boolean;
    isForSale?:            boolean;
    area?:                 string;
    verified?:             boolean;
    representativeId?:     number;
    propertyTypeId?:       number;
    propertyType?:         PropertyType;
    createdByUserId?:      number;
    mediaFiles?:           PassportPhotograph[];
    isRequest?:            boolean;
    statusId?:             number;
    status?:               PropertyType;
    reports?:              Report[];
    views?:                number;
    enquiries?:            number;
    longitude?:            number;
    latitude?:             number;
    userEnquiries?:        null[];
    tenantTypeId?:         number;
    tenantType?:           PropertyType;
    rentCollectionType?:   PropertyType;
    rentCollectionTypeId?: number;
    inspections?:          Inspection[];
}

export interface Inspection {
    id?:               number;
    dateCreated?:      Date;
    dateModified?:     Date;
    inspectionDateId?: number;
    inspectionTimeId?: number;
    inspectionDate?:   InspectionDate;
    inspectionTime?:   Time;
    userId?:           number;
    propertyId?:       number;
}

export interface InspectionDate {
    id?:           number;
    dateCreated?:  Date;
    dateModified?: Date;
    propertyId?:   number;
    date?:         Date;
    times?:        Time[];
    inspections?:  null[];
}

export interface Time {
    id?:               number;
    dateCreated?:      Date;
    dateModified?:     Date;
    inspectionDateId?: number;
    availableTime?:    Date;
    isAvailable?:      boolean;
}

export interface PassportPhotograph {
    id?:           number;
    dateCreated?:  Date;
    dateModified?: Date;
    name?:         string;
    extention?:    string;
    url?:          string;
    base64String?: string;
    propertyId?:   number;
    isImage?:      boolean;
    isVideo?:      boolean;
    isDocument?:   boolean;
}

export interface PropertyType {
    id?:           number;
    dateCreated?:  Date;
    dateModified?: Date;
    name?:         string;
}

export interface Report {
    id?:           number;
    dateCreated?:  Date;
    dateModified?: Date;
    propertyId?:   number;
    userId?:       number;
    description?:  number;
}

export interface Application {
    id?:                 number;
    dateCreated?:        Date;
    dateModified?:       Date;
    userId?:             number;
    propertyId?:         number;
    property?:           Property;
    nextOfKinId?:        number;
    nextOfKin?:          NextOfKin;
    applicationTypeId?:  number;
    applicationType?:    PropertyType;
    statusId?:           number;
    status?:             PropertyType;
    reliefAmount?:       number;
    payBackDate?:        Date;
    repaymentFrequency?: string;
}

export interface NextOfKin {
    id?:           number;
    dateCreated?:  Date;
    dateModified?: Date;
    firstName?:    string;
    lastName?:     string;
    middleName?:   string;
    email?:        string;
    phoneNumber?:  string;
    relationship?: string;
    address?:      string;
}

export interface Cleaning {
    id?:                number;
    dateCreated?:       Date;
    dateModified?:      Date;
    buildingState?:     string;
    propertyTypeId?:    number;
    propertyType?:      PropertyType;
    dateNeeded?:        Date;
    numberOfBathrooms?: number;
    numberOfBedrooms?:  number;
    numberOfFloors?:    number;
    buildingType?:      string;
    userId?:            number;
    statusId?:          number;
    status?:            PropertyType;
    cleaningQuotes?:    CleaningQuote[];
}

export interface CleaningQuote {
    id?:           number;
    dateCreated?:  Date;
    dateModified?: Date;
    quote?:        number;
    proposedDate?: Date;
    statusId?:     number;
    status?:       PropertyType;
    cleaningId?:   number;
}

export interface Code {
    id?:           number;
    dateCreated?:  Date;
    dateModified?: Date;
    codeString?:   string;
    key?:          string;
    userId?:       number;
    expiryDate?:   Date;
    isExpired?:    boolean;
    token?:        string;
}

export interface Installment {
    id?:           number;
    dateCreated?:  Date;
    dateModified?: Date;
    amount?:       number;
    dateDue?:      Date;
    statusId?:     number;
    status?:       PropertyType;
    rentReliefId?: number;
}
