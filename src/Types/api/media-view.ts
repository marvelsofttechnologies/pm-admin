/* tslint:disable */
export interface MediaView {
  url?: null | string;
  isImage?: boolean;
  isVideo?: boolean;
  isDocument?: boolean;
}
