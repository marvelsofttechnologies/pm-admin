/* tslint:disable */
import { Property } from './property';

export interface Media {
  id?: number;
  dateCreated?: string;
  dateModified?: string;
  name: string;
  extention: string;
  url: string;
  base64String?: null | string;
  propertyId?: null | number;
  isImage?: boolean;
  isVideo?: boolean;
  isDocument?: boolean;
  property?: Property;
}
