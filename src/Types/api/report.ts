/* tslint:disable */
export interface Report {
  id?: number;
  dateCreated?: string;
  dateModified?: string;
  propertyId?: number;
  userId?: number;
  description?: number;
}
